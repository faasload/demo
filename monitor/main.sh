#!/usr/bin/env bash

FAASLOAD_HOME="$HOME/faasload"

function pause() {
    echo -n "Entrée pour continuer..."
    read
    echo
}

runtime_docker_id="$(docker ps -a | awk '/wsk[0-9]{2}_[0-9]+_guest_sharp_blur/ { print $1 }')"
if [ -n "$runtime_docker_id" ]; then
    ( docker stop  $runtime_docker_id && docker rm $runtime_docker_id ) >/dev/null
fi
( docker stop faasload-monitor && docker rm faasload-monitor ) 2>&1 >/dev/null

echo "Configuration du moniteur"
echo "-------------------------------------------------------------------------"
pause

vim "$FAASLOAD_HOME/config/monitor.yml"

echo "-------------------------------------------------------------------------"
echo

echo "Lancement du moniteur"
echo -e "\tIl est conteneurisé (Docker), et certaines choses doivent lui être exposées :"
echo -e "\t* sa configuration"
echo -e "\t* l'interface des control groups, pour le monitoring"
echo -e "\t* le socket Docker, pour surveiller le cycle de vie des conteneurs"
echo -e "\tIl faut aussi donner la capacité \"CAP_SYS_ADMIN\" pour le monitoring avec perf."
echo "-------------------------------------------------------------------------"
echo "$ docker run --detach --name faasload-monitor\\
    --publish 11085:11085\\
    --volume $FAASLOAD_HOME/config/monitor.yml:/home/faasload/.config/faasload/monitor.yml\\
    --volume /sys/fs/cgroup/memory:/sys/fs/cgroup/memory\\
    --volume /sys/fs/cgroup/cpu,cpuacct:/sys/fs/cgroup/cpu,cpuacct\\
    --volume /sys/fs/cgroup/perf_event:/sys/fs/cgroup/perf_event\\
    --volume /var/run/docker.sock:/var/run/docker.sock\\
    --cap-add SYS_ADMIN\\
    faasload/monitor"

docker run --detach --name faasload-monitor\
    --publish 11085:11085\
    --volume "$FAASLOAD_HOME/config/monitor.yml:/home/faasload/.config/faasload/monitor.yml"\
    --volume /sys/fs/cgroup/memory:/sys/fs/cgroup/memory\
    --volume /sys/fs/cgroup/cpu,cpuacct:/sys/fs/cgroup/cpu,cpuacct\
    --volume /sys/fs/cgroup/perf_event:/sys/fs/cgroup/perf_event\
    --volume /var/run/docker.sock:/var/run/docker.sock\
    --cap-add SYS_ADMIN\
    faasload/monitor

# Il faut attendre un peu sinon le monitor rate la création du conteneur pour
# l'invocation qui vient ensuite.
sleep 2

echo "-------------------------------------------------------------------------"
echo

echo "Invocation d'une action OpenWhisk (pour avoir quelque chose à monitorer !)"
echo "-------------------------------------------------------------------------"
echo "$ wsk action invoke --blocking faasload/sharp_blur\\
    --param incont image --param outcont sharp-blur_out --param object 1.jpg\\
    --param sigma 0.5"

wsk -i action invoke --blocking faasload/sharp_blur\
    --param incont image --param outcont sharp-blur_out --param object 1.jpg\
    --param sigma 0.5

echo "-------------------------------------------------------------------------"
echo

pause

echo "Logs du moniteur"
echo -e "\tIl indique qu'il a vu un conteneur démarrer."
echo -e "\tIl peut aussi indiquer la « santé » de son monitoring."
echo "-------------------------------------------------------------------------"
pause

docker logs faasload-monitor 2>&1 | less

echo "-------------------------------------------------------------------------"
echo

# « Brûler » la première commande (bug de wsk qui ne récupère pas la liste des
# activations à jour)
wsk -i activation list >/dev/null
act_id=$(wsk -i activation get -l | tail -n+2 | jq -r '.activationId')

if [ -z "$act_id" ]; then
    echo "Échec de la récupération de l'ID d'activation" >&2
    exit 1
fi

echo "Récupération manuelle des données relevées"
echo -e "\tScript pour tester manuellement le moniteur."
echo -e "\tIl faut d'abord récupérer l'ID de l'activation chez OpenWhisk."
echo -e "\tL'action a été invoquée dans le namespace OpenWhisk par défaut, noté \"_\"."
echo -e "\tIl utilise le conteneur du moniteur, et a besoin de certaines choses :"
echo -e "\t* la configuration de l'injecteur (le client habituel du moniteur)"
echo -e "\t* le jeton d'authentification pour récupérer les informations de l'activation"
echo -e "\t* les logs d'OpenWhisk pour faire la correspondance entre l'ID de l'activation et le conteneur Docker"
echo "-------------------------------------------------------------------------"
echo "$ docker run --rm\\
    --volume $FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml\\
    --volume $FAASLOAD_HOME/users:/home/faasload/users\\
    --volume ~/openwhisk:/home/faasload/openwhisk\\
    faasload/monitor docker-monitor-client.py\\
    _ $act_id"

docker run --rm\
    --volume "$FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml"\
    --volume "$FAASLOAD_HOME/users:/home/faasload/faasload/users"\
    --volume ~/openwhisk:/home/faasload/openwhisk\
    faasload/monitor docker-monitor-client.py\
    _ "$act_id"

echo "-------------------------------------------------------------------------"
echo

pause

