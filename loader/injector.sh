#! /usr/bin/env bash

FAASLOAD_HOME="$HOME/faasload"

function pause() {
    echo -n "Entrée pour continuer..."
    read
    echo
}

docker run --interactive --tty --rm --network faasload\
  mariadb\
  mysql --host mysql --user faasload --password=faasload faasload\
  -e "drop table parameters; drop table resources; drop table results; drop table runs;"\
  >/dev/null

( docker stop faasload-monitor && docker rm faasload-monitor ) >/dev/null 2>/dev/null

docker run --rm --network faasload\
    --volume "$FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml"\
    --volume ~/actions:/home/faasload/actions\
    --volume "$FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces"\
    --volume "$FAASLOAD_HOME/users:/home/faasload/faasload/users"\
    faasload/loader load-wsk-assets.py --unload\
        /home/faasload/actions/manifest.yml\
        --param-file /home/faasload/actions/swift_parameters.json\
        --injector /home/faasload/faasload/injection-traces/sample\
    >/dev/null 2>/dev/null

echo "Configuration de l'injecteur"
echo "-------------------------------------------------------------------------"
pause

vim "$FAASLOAD_HOME/config/loader.yml"

echo "-------------------------------------------------------------------------"
echo

docker run --detach --name faasload-monitor\
    --publish 11085:11085\
    --volume "$FAASLOAD_HOME/config/monitor.yml:/home/faasload/.config/faasload/monitor.yml"\
    --volume /sys/fs/cgroup/memory:/sys/fs/cgroup/memory\
    --volume /sys/fs/cgroup/cpu,cpuacct:/sys/fs/cgroup/cpu,cpuacct\
    --volume /sys/fs/cgroup/perf_event:/sys/fs/cgroup/perf_event\
    --volume /var/run/docker.sock:/var/run/docker.sock\
    --cap-add SYS_ADMIN\
    faasload/monitor\
    >/dev/null

echo "Préparation des ressources OpenWhisk : utilisateurs et actions"
echo -e "\tUtilisation du conteneur de l'injecteur, et il faut fournir certaines choses :"
echo -e "\t* la configuration de l'injecteur pour lui préparer ses ressources"
echo -e "\t* les actions utilisées dans les traces d'injection de charge"
echo -e "\t* les traces d'injection de charges"
echo -e "\t* le dossier où écrire les jetons d'authentification des utilisateurs des traces"
echo "-------------------------------------------------------------------------"
echo "$ docker run --rm --network faasload\\
    --volume $FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml\\
    --volume $HOME/actions:/home/faasload/actions\\
    --volume $FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces\\
    --volume $FAASLOAD_HOME/faasload/users:/home/faasload/faasload/users\\
    faasload/loader load-wsk-assets.py\\
        /home/faasload/actions/manifest.yml\\
        --param-file /home/faasload/actions/swift_parameters.json\\
        --injector injection-traces/samples"

docker run --rm --network faasload\
    --volume "$FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml"\
    --volume ~/actions:/home/faasload/actions\
    --volume "$FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces"\
    --volume "$FAASLOAD_HOME/users:/home/faasload/faasload/users"\
    faasload/loader load-wsk-assets.py\
        /home/faasload/actions/manifest.yml\
        --param-file /home/faasload/actions/swift_parameters.json\
        --injector /home/faasload/faasload/injection-traces/sample

echo "-------------------------------------------------------------------------"
echo

pause

echo "Injection des traces de charges"
echo -e "\tL'injecteur est conteneurisé (Docker), et certaines choses doivent lui être exposées :"
echo -e "\t* sa configuration"
echo -e "\t* les logs d'OpenWhisk pour faire la correspondance entre les ID des activations et le conteneur Docker"
echo -e "\t* les traces d'injection de charge"
echo -e "\t* les jetons d'authentification des utilisateurs des traces"
echo "-------------------------------------------------------------------------"
echo "$ docker run --rm --network faasload --name faasload-loader\\
    --volume $FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml\\
    --volume $HOME/openwhisk:/home/faasload/openwhisk\
    --volume $FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces\\
    --volume $FAASLOAD_HOME/users:/home/faasload/faasload/users\\
    faasload/loader"

docker run --rm --network faasload --name faasload-loader\
    --volume "$FAASLOAD_HOME/config/loader.yml:/home/faasload/.config/faasload/loader.yml"\
    --volume ~/openwhisk:/home/faasload/openwhisk\
    --volume "$FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces"\
    --volume "$FAASLOAD_HOME/users:/home/faasload/faasload/users"\
    faasload/loader

echo "-------------------------------------------------------------------------"
echo

pause

echo "Jeu de données produit par l'injection des traces de charge"
echo "-------------------------------------------------------------------------"
pause

docker run --interactive --tty --rm --network faasload\
  mariadb\
  mysql --host mysql --user faasload --password=faasload faasload

echo "-------------------------------------------------------------------------"
echo

