#! /usr/bin/env bash

FAASLOAD_HOME="$HOME/faasload"

function pause() {
    echo -n "Entrée pour continuer..."
    read
    echo
}

rm -r "$FAASLOAD_HOME/injection-traces/sample"

echo "Spécification des traces d'injection de charge"
echo "-------------------------------------------------------------------------"
pause

vim "$FAASLOAD_HOME/injection-user-specs/sample.yml"

echo "-------------------------------------------------------------------------"
echo

echo "Construction des traces d'injection de charge"
echo -e "\tUtilisation du conteneur de l'injecteur, il faut donc fournir certaines choses :"
echo -e "\t* les actions OpenWhisk utilisées dans les traces (décrites dans le manifest.yml)"
echo -e "\t* la spécification des traces d'injection de charge"
echo -e "\t* le dossier où écrire les traces construites"
echo "-------------------------------------------------------------------------"
echo "$ docker run --rm\\
    --volume ~/actions:/home/faasload/actions\\
    --volume $FAASLOAD_HOME/injection-user-specs:/home/faasload/faasload/injection-user-specs\\
    --volume $FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces\\
    faasload/loader trace-builder.py\\
        /home/faasload/actions/manifest.yml\\
        --duration 300\\
        --user-specs /home/faasload/faasload/injection-user-specs/sample.yml\\
        --random-seed 20191108"

docker run --rm\
    --volume ~/actions:/home/faasload/actions\
    --volume "$FAASLOAD_HOME/injection-user-specs:/home/faasload/faasload/injection-user-specs"\
    --volume "$FAASLOAD_HOME/injection-traces:/home/faasload/faasload/injection-traces"\
    faasload/loader trace-builder.py\
        /home/faasload/actions/manifest.yml\
        --duration 300\
        --user-specs /home/faasload/faasload/injection-user-specs/sample.yml\
        --random-seed 20191108

echo "-------------------------------------------------------------------------"
echo

pause

echo "Traces d'injection"
echo "-------------------------------------------------------------------------"
pause

vim "$FAASLOAD_HOME/injection-traces/sample"

echo "-------------------------------------------------------------------------"
echo

